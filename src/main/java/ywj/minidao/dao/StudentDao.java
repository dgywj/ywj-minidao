package ywj.minidao.dao;

import org.jeecgframework.minidao.annotation.Param;
import org.jeecgframework.minidao.annotation.Sql;
import org.jeecgframework.minidao.hibernate.MiniDaoSupportHiber;
import org.springframework.stereotype.Repository;

import ywj.minidao.entity.Student;

@Repository
public interface StudentDao extends MiniDaoSupportHiber<Student>{

	/**
	 * 查询返回Java对象
	 * @param id
	 * @return
	 */
	@Sql("select * from student where id = :id")
	Student get(@Param("id") String id);
	
	/**
	 * 修改数据
	 * @param employee
	 * @return
	 */
	int update(@Param("student") Student student);
	
	/**
	 * 插入数据
	 * @param employee
	 */
	void insert(@Param("student") Student student);
	
}
