package ywj.minidao.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ywj.minidao.dao.StudentDao;
import ywj.minidao.entity.Student;

public class MiniDaoTest {

	StudentDao studentDao = null;
	
	@Before
	public void initDao() {
		Student student = new Student();
		BeanFactory factory = new ClassPathXmlApplicationContext("applicationContext.xml");

		studentDao = (StudentDao) factory.getBean("studentDao");
	}

	@Test
	public void testInsert() {
		Integer age = new Integer(20);
		Student student = new Student("002", "ywj", age);
		studentDao.insert(student);

	}
	
	@Test
	public void testUpdate() {
		Integer age = new Integer(22);
		Student student = new Student("002", "ywj2", age);
		studentDao.update(student);

	}
	
	@Test
	public void testSelect() {
		Student student = studentDao.get("002");
		System.out.println(student.getName());

	}

}
